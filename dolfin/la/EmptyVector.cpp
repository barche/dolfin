// Copyright (C) 2006-2008 Garth N. Wells
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Anders Logg 2006-2012
// Modified by Kent-Andre Mardal 2008
// Modified by Martin Sandve Alnes 2008
//
// First added:  2006-04-04
// Last changed: 2012-03-15

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <boost/unordered_set.hpp>

#include <dolfin/log/dolfin_log.h>
#include <dolfin/common/Timer.h>
#include <dolfin/common/Array.h>
#include "EmptyVector.h"
#include "EmptyFactory.h"
#include "GenericLinearAlgebraFactory.h"

using namespace dolfin;

//-----------------------------------------------------------------------------
EmptyVector::~EmptyVector()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
boost::shared_ptr<GenericVector> EmptyVector::copy() const
{
  boost::shared_ptr<GenericVector> y(new EmptyVector());
  return y;
}
//-----------------------------------------------------------------------------
void EmptyVector::resize(std::size_t N)
{
}
//-----------------------------------------------------------------------------
void EmptyVector::resize(std::pair<std::size_t, std::size_t> range)
{
}
//-----------------------------------------------------------------------------
void EmptyVector::resize(std::pair<std::size_t, std::size_t> range,
                         const std::vector<la_index>& ghost_indices)
{
}
//-----------------------------------------------------------------------------
bool EmptyVector::empty() const
{
  return true;
}
//-----------------------------------------------------------------------------
std::size_t EmptyVector::size() const
{
  return 0;
}
//-----------------------------------------------------------------------------
std::pair<std::size_t, std::size_t> EmptyVector::local_range() const
{
  return std::make_pair(0, 0);
}
//-----------------------------------------------------------------------------
bool EmptyVector::owns_index(std::size_t i) const
{
  return false;
}
//-----------------------------------------------------------------------------
void EmptyVector::get_local(double* block, std::size_t m,
                            const dolfin::la_index* rows) const
{
}
//-----------------------------------------------------------------------------
void EmptyVector::get_local(std::vector<double>& values) const
{
}
//-----------------------------------------------------------------------------
void EmptyVector::set_local(const std::vector<double>& values)
{
}
//-----------------------------------------------------------------------------
void EmptyVector::add_local(const Array<double>& values)
{
}
//-----------------------------------------------------------------------------
void EmptyVector::gather(GenericVector& x,
                         const std::vector<dolfin::la_index>& indices) const
{
}
//-----------------------------------------------------------------------------
void EmptyVector::gather(std::vector<double>& x,
                         const std::vector<dolfin::la_index>& indices) const
{
}
//-----------------------------------------------------------------------------
void EmptyVector::gather_on_zero(std::vector<double>& x) const
{
}
//-----------------------------------------------------------------------------
void EmptyVector::set(const double* block, std::size_t m,
                      const dolfin::la_index* rows)
{
}
//-----------------------------------------------------------------------------
void EmptyVector::add(const double* block, std::size_t m,
                      const dolfin::la_index* rows)
{
}
//-----------------------------------------------------------------------------
void EmptyVector::apply(std::string mode)
{
}
//-----------------------------------------------------------------------------
void EmptyVector::zero()
{
}
//-----------------------------------------------------------------------------
double EmptyVector::norm(std::string norm_type) const
{
  return 0.0;
}
//-----------------------------------------------------------------------------
double EmptyVector::min() const
{
  return 0.0;
}
//-----------------------------------------------------------------------------
double EmptyVector::max() const
{
  return 0.0;
}
//-----------------------------------------------------------------------------
double EmptyVector::sum() const
{
  return 0.0;
}
//-----------------------------------------------------------------------------
double EmptyVector::sum(const Array<std::size_t>& rows) const
{
  return 0.0;
}
//-----------------------------------------------------------------------------
void EmptyVector::axpy(double a, const GenericVector& y)
{
}
//-----------------------------------------------------------------------------
void EmptyVector::abs()
{
}
//-----------------------------------------------------------------------------
double EmptyVector::inner(const GenericVector& y) const
{
  return 0.0;
}
//-----------------------------------------------------------------------------
const GenericVector& EmptyVector::operator= (const GenericVector& v)
{
  return *this;
}
//-----------------------------------------------------------------------------
const EmptyVector& EmptyVector::operator= (double a)
{
  return *this;
}
//-----------------------------------------------------------------------------
const EmptyVector& EmptyVector::operator*= (const double a)
{
  return *this;
}
//-----------------------------------------------------------------------------
const EmptyVector& EmptyVector::operator*= (const GenericVector& y)
{
  return *this;
}
//-----------------------------------------------------------------------------
const EmptyVector& EmptyVector::operator/= (const double a)
{
  return *this;
}
//-----------------------------------------------------------------------------
const EmptyVector& EmptyVector::operator+= (const GenericVector& y)
{
  return *this;
}
//-----------------------------------------------------------------------------
const EmptyVector& EmptyVector::operator+= (double a)
{
  return *this;
}
//-----------------------------------------------------------------------------
const EmptyVector& EmptyVector::operator-= (const GenericVector& y)
{
  return *this;
}
//-----------------------------------------------------------------------------
const EmptyVector& EmptyVector::operator-= (double a)
{
  return *this;
}
//-----------------------------------------------------------------------------
std::string EmptyVector::str(bool verbose) const
{
    return "EmptyVector";
}
//-----------------------------------------------------------------------------
GenericLinearAlgebraFactory& EmptyVector::factory() const
{
  return EmptyFactory::instance();
}
//-----------------------------------------------------------------------------
