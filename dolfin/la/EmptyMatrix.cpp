// Copyright (C) 2007-2012 Anders Logg and Garth N. Wells
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Ola Skavhaug 2007
// Modified by Ilmar Wilbers 2008
//
// First added:  2007-01-17
// Last changed: 2012-03-15


#include <dolfin/common/types.h>
#include "EmptyFactory.h"
#include "EmptyMatrix.h"

using namespace dolfin;

//-----------------------------------------------------------------------------
void EmptyMatrix::init(const TensorLayout& tensor_layout)
{
}
//-----------------------------------------------------------------------------
std::size_t EmptyMatrix::size(std::size_t dim) const
{
  return 0;
}
//-----------------------------------------------------------------------------
std::pair<std::size_t, std::size_t>
EmptyMatrix::local_range(std::size_t dim) const
{
    return std::make_pair(0u, 0u);
}
//-----------------------------------------------------------------------------
void EmptyMatrix::zero()
{
}
//-----------------------------------------------------------------------------
void EmptyMatrix::add(const double* block,
                    std::size_t m, const dolfin::la_index* rows,
                    std::size_t n, const dolfin::la_index* cols)
{
}
//-----------------------------------------------------------------------------
void EmptyMatrix::apply(std::string mode)
{
}
//-----------------------------------------------------------------------------
double EmptyMatrix::norm(std::string norm_type) const
{
  return 0.;
}
//-----------------------------------------------------------------------------
void EmptyMatrix::getrow(std::size_t row, std::vector<std::size_t>& columns,
                       std::vector<double>& values) const
{
}
//-----------------------------------------------------------------------------
void EmptyMatrix::ident(std::size_t m, const dolfin::la_index* rows)
{
}
//-----------------------------------------------------------------------------
const EmptyMatrix& EmptyMatrix::operator*= (double a)
{
  return *this;
}
//-----------------------------------------------------------------------------
const EmptyMatrix& EmptyMatrix::operator/= (double a)
{
  return *this;
}
//-----------------------------------------------------------------------------
std::string EmptyMatrix::str(bool verbose) const
{
  return "EmptyMatrix";
}
//-----------------------------------------------------------------------------
GenericLinearAlgebraFactory& EmptyMatrix::factory() const
{
    return EmptyFactory::instance();
}
//-----------------------------------------------------------------------------
