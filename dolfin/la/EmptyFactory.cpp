// Copyright (C) 2008-2011 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
//
// First added:  2013-08-31

#include <dolfin/parameter/GlobalParameters.h>
#include "EmptyFactory.h"
#include "EmptyMatrix.h"
#include "EmptyVector.h"

using namespace dolfin;

//-----------------------------------------------------------------------------
boost::shared_ptr<GenericMatrix> EmptyFactory::create_matrix() const
{
  return boost::shared_ptr<EmptyMatrix>(new EmptyMatrix());
}
//-----------------------------------------------------------------------------
boost::shared_ptr<GenericVector> EmptyFactory::create_vector() const
{
  return boost::shared_ptr<EmptyVector>(new EmptyVector());
}
//-----------------------------------------------------------------------------
boost::shared_ptr<GenericVector> EmptyFactory::create_local_vector() const
{
  return boost::shared_ptr<EmptyVector>(new EmptyVector());
}
//-----------------------------------------------------------------------------
boost::shared_ptr<TensorLayout> EmptyFactory::create_layout(std::size_t rank) const
{
  boost::shared_ptr<TensorLayout> pattern(new TensorLayout(0, false));
  return pattern;
}
//-----------------------------------------------------------------------------
boost::shared_ptr<GenericLinearOperator> EmptyFactory::create_linear_operator() const
{
  boost::shared_ptr<GenericLinearOperator> A(new NotImplementedLinearOperator);
  return A;
}
//-----------------------------------------------------------------------------
boost::shared_ptr<GenericLUSolver>
  EmptyFactory::create_lu_solver(std::string method) const
{
  boost::shared_ptr<GenericLUSolver> solver;
  return solver;
}
//-----------------------------------------------------------------------------
boost::shared_ptr<GenericLinearSolver>
  EmptyFactory::create_krylov_solver(std::string method,
                                     std::string preconditioner) const
{
  boost::shared_ptr<GenericLinearSolver> solver;
  return solver;
}
//-----------------------------------------------------------------------------
